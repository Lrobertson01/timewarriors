#include "LevelClass.h"
#include "Game.h"
#include <stdlib.h>
#include <time.h>
#include "AssetManager.h"

LevelClass::LevelClass(Game* newGamePointer)
    :playerInstance(newGamePointer->windowGet().getSize())
    , gamePointer(newGamePointer)
    , transition(AssetManager::RequestTexture("Assets/Backgrounds/Portal.png"))
    , turn(true)
    , attackBox("Attack.png")
    , guardBox("Guard.png")
    , skillBox("Skills.png")
    , itemBox("Items.png")
    , lightBox("Light.png")
    , heavyBox("Heavy.png")
    , counterBox("Counter.png")
    , backBox("Back.png")
    , healthBox("Health.png")
    , SPBox("SP.png")
    , startBox("Start.png")
    , continueBox("Continue.png")
    , quitBox("Quit.png")
    , isLevel(false)
    , isFighting(false)
    , currentLevel(0)
    , currentFight(-1)
    , ambientMusic()
    , battleMusic()
    , gameOver(false)

{
    spawnEnemy("Gladiator");
    spawnEnemy("Roman");
    spawnEnemy("Ares");
    spawnEnemy("Archer");
    spawnEnemy("Knight");
    spawnEnemy("Ares");
    spawnEnemy("Pikeman");
    spawnEnemy("MusketMan");
    spawnEnemy("Ares");

    background.push_back(SpriteObject(AssetManager::RequestTexture("Assets/Backgrounds/RomeBackground.jpg")));
    background.push_back(SpriteObject(AssetManager::RequestTexture("Assets/Backgrounds/BannockburnBackground.jpg")));
    background.push_back(SpriteObject(AssetManager::RequestTexture("Assets/Backgrounds/WaterlooBackground.png")));

    //Set the positions of each of the input boxes on the screen
    sf::Vector2u screenSize = gamePointer->windowGet().getSize();

    sf::Vector2f newPosition = sf::Vector2f(screenSize.x / 2 - attackBox.getHitbox().width * 1.5 , screenSize.y / 2 + attackBox.getHitbox().height *2);
    attackBox.setPosition(newPosition);
    lightBox.setPosition(newPosition);

    //use attack box in all position calculations to keep it consistent, each box has a slightly different width and so we cant just use the box we are currently setting
    newPosition.y += attackBox.getHitbox().height *2;
    guardBox.setPosition(newPosition);
    heavyBox.setPosition(newPosition);
    SPBox.setPosition(newPosition);

    newPosition.x += attackBox.getHitbox().width * 2;
    itemBox.setPosition(newPosition);
    backBox.setPosition(newPosition);

    newPosition.y -= attackBox.getHitbox().height *2;
    skillBox.setPosition(newPosition);
    counterBox.setPosition(newPosition);
    healthBox.setPosition(newPosition);

    playerInstance.reset(); //Set the player to be ready when the fight starts

    ambientMusic.openFromFile("Assets/Audio/AmbientMusic.ogg");
    ambientMusic.setLoop(true);
    battleMusic.openFromFile("Assets/Audio/RomeMusic.ogg");
    battleMusic.setLoop(true);
    ambientMusic.play(); //Set the game to play ambient music at the start so its on the menu

        //The reason font is set manually upon each instance of 
    winText.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
    winText.setString("Congrats you win, press continue to move on to the next battle");
    winText.setCharacterSize(24);
    winText.setFillColor(sf::Color::White);
    winText.setPosition(100, gamePointer->windowGet().getSize().y / 10);

    loseText.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
    loseText.setString("You lost, press continue to try again and restart the fight");
    loseText.setCharacterSize(24);
    loseText.setFillColor(sf::Color::White);
    loseText.setPosition(100, gamePointer->windowGet().getSize().y / 10);

    gameWon.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
    gameWon.setString("You have won the game, press the quit button to leave");
    gameWon.setCharacterSize(24);
    gameWon.setFillColor(sf::Color::White);
    gameWon.setPosition(100, gamePointer->windowGet().getSize().y / 10);
}

void LevelClass::input()
{
    if (isLevel)
    {
        if (isFighting)
        {
            if (turn == true && mouselag < 0)
            {
                if (playerInstance.getAction() == "None")
                {
                    playerInstance.Input(skillBox.getHitbox(), "Skills", 1, 0, false, 0);
                    playerInstance.Input(attackBox.getHitbox(), "None", 1, 100, true, -1);
                    playerInstance.Input(itemBox.getHitbox(), "Items", 1, 0, false, 0);
                    playerInstance.Input(guardBox.getHitbox(), "None", 0.5, 0, true, -1);
                    if (playerInstance.hasClicked())
                    {
                        mouselag = 0.25;
                    }
                }
                //Using an else if as otherwise it will automatically click on the option in the same place in the skills menu
                else if (playerInstance.getAction() == "Skills")
                {
                    playerInstance.Input(lightBox.getHitbox(), "Light", 1, 200, true, 2);
                    playerInstance.Input(heavyBox.getHitbox(), "Heavy", 1, 400, true, 4);
                    playerInstance.Input(counterBox.getHitbox(), "Heavy", 0, 500, true, 6);
                    playerInstance.Input(backBox.getHitbox(), "None", 1, 0, false, 0);
                    if (playerInstance.hasClicked())
                    {
                        mouselag = 0.2;
                    }
                }

                else if (playerInstance.getAction() == "Items")
                {
                    playerInstance.Input(healthBox.getHitbox(), "Heal", 1, 0, true, 3);
                    playerInstance.Input(SPBox.getHitbox(), "Items", 1, 0, true, -2);
                    playerInstance.Input(backBox.getHitbox(), "None", 1, 0, false, 0);
                    if (playerInstance.hasClicked())
                    {
                        mouselag = 0.2;
                    }
                }

            }
        }

        if (!isFighting)
        {
            playerInstance.Input(continueBox.getHitbox(), "None", 1, 0, false, 0);
        }
    }

    
    if (!isLevel)
    {
        if (!gameOver)
        {
            playerInstance.Input(startBox.getHitbox(), "None", 1, 0, false, 0);
        }
        if (gameOver)
        {
            playerInstance.Input(quitBox.getHitbox(), "None", 1, 0, false, 0);
        }
    }

    
}

void LevelClass::update(sf::Time GameTime)
{
    if (isFighting)
    {
        if (playerInstance.getAction() == "Heal")
        {
            playerInstance.Heal(500);
        }

        if (playerInstance.getHealth() <= 0)
        {
            isFighting = false; 
        }
        playerInstance.Update();
        enemyInstances[currentFight].Update();

        mouselag -= GameTime.asSeconds();

        if (playerInstance.checkAction())
        {
            enemyInstances[currentFight].takeDamage(playerInstance.getDamage());
            turn = false;
        }

        if (turn == false)
        {
            playerInstance.takeDamage(enemyInstances[currentFight].Attack());
            turn = true;
            playerInstance.reset();
            //Reset the player at the start of the turn
        }
        
        //End the fight once the enemy reaches 0 health
        if (enemyInstances[currentFight].getHealth() <= 0)
        {
            isFighting = false;


            if (currentFight + 1 == 3 || currentFight + 1 == 6)
            {
                isLevel = false;
                currentLevel++;
                battleMusic.stop();
                ambientMusic.play();
            }

            else if (currentFight + 1 == 9)
            {
                isLevel = (false);
                gameOver = true;
            }
        }
    }

    if (!isFighting)
    {
        if (!gameOver)
        {
            if (playerInstance.hasClicked())
            {
                isLevel = true;
                isFighting = true;
                if (playerInstance.getHealth() >= 0)
                {
                    //Only advance the fight if the player is positive in health (AKA won), or is not the final fight
                    currentFight++;
                }
                else
                {
                    enemyInstances[currentFight].restoreHealth(); //Reset the enemy to max health when the fight is restarted
                }
                battleMusic.play();
                ambientMusic.stop();
                playerInstance.Heal(2000); //reset the player
                playerInstance.fullSkills();
                playerInstance.reset();
            }
        }

        if (gameOver)
        {
            if (playerInstance.hasClicked())
            {
                gamePointer->windowGet().close(); //Close the window and end the game once the final boss has been beaten
            }
        }
    }

    
}

void LevelClass::drawTo(sf::RenderTarget& Target)
{   
    if (isLevel)
    {
        background[currentLevel].DrawTo(Target);
        playerInstance.DrawTo(Target);
        if (isFighting)
        {
            enemyInstances[currentFight].DrawTo(Target);
        }

        if (!isFighting)
        {
            continueBox.DrawTo(Target);
            if (playerInstance.getHealth() > 0)
            {
                Target.draw(winText);
            }
            else
            {
                Target.draw(loseText);
            }
        }
        

        if (playerInstance.getAction() == "None")
        {
            attackBox.DrawTo(Target);
            guardBox.DrawTo(Target);
            itemBox.DrawTo(Target);
            skillBox.DrawTo(Target);
        }
        if (playerInstance.getAction() == "Skills")
        {
            lightBox.DrawTo(Target);
            heavyBox.DrawTo(Target);
            counterBox.DrawTo(Target);
            backBox.DrawTo(Target);
        }

        if (playerInstance.getAction() == "Items")
        {
            healthBox.DrawTo(Target);
            SPBox.DrawTo(Target);
            backBox.DrawTo(Target);
        }
    }


    if (!isLevel)
    {
        transition.DrawTo(Target);
        if (gameOver == false)
        {
            startBox.DrawTo(Target);
        }
        if (gameOver == true)
        {
            Target.draw(gameWon);
            quitBox.DrawTo(Target);
        }
    }
}

void LevelClass::spawnEnemy(sf::String enemyName)
{
    sf::Vector2u screensize = gamePointer->windowGet().getSize();
    enemyInstances.push_back(Enemy(screensize, enemyName));
}


