#pragma once
#include "SpriteObject.h"


class SelectBox :
    public SpriteObject
{
public:
    SelectBox(std::string fileName);
    void setPosition(sf::Vector2f newPosition);
};

