#include "SelectBox.h"
#include "AssetManager.h"

SelectBox::SelectBox(std::string fileName)
	:SpriteObject(AssetManager::RequestTexture("Assets/UI/" + fileName))
{
	
}

void SelectBox::setPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}
