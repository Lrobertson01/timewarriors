#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u Screensize)
	:SpriteObject(AssetManager::RequestTexture("Assets/UI/UIBox.png"))
	, currentAction("None")
	, takenAction(false)
	, health(1000)
	, gameFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"))
	, DR(1)
	, attackDamage(100)
	, skillPoints(10)
	, lightSound(AssetManager::RequestSound("Assets/Audio/LightAttack.wav"))
	, heavySound(AssetManager::RequestSound("Assets/Audio/HeavyAttack.wav"))
	, counterSound(AssetManager::RequestSound("Assets/Audio/CounterAttack.wav"))

{
	sprite.setPosition(sf::Vector2f(Screensize.x / 2 - sprite.getGlobalBounds().width/2, Screensize.y - sprite.getGlobalBounds().height));

	healthText.setFont(gameFont);
	healthText.setString("Health = 750");
	healthText.setCharacterSize(32);
	healthText.setFillColor(sf::Color::White);
	healthText.setPosition(Screensize.x / 10, Screensize.y / 1.7);

	skillText.setFont(gameFont);
	skillText.setString("Skill points: 10");
	skillText.setCharacterSize(32);
	skillText.setFillColor(sf::Color::White);
	skillText.setPosition(Screensize.x / 1.5, Screensize.y / 1.7);

	heavyAttackEffect.setBuffer(heavySound);
	lightAttackEffect.setBuffer(lightSound);
	counterAttackEffect.setBuffer(counterSound);
}

void Player::Input(sf::FloatRect newBox, sf::String currentBox, float NewDR, float damage, bool actionender, int SPcost)
{
	sf::Vector2f mousePos(sf::Mouse::getPosition());
	sf::FloatRect MouseRect(mousePos.x, mousePos.y, 1,1);

	if (newBox.intersects(MouseRect) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && skillPoints >= SPcost)
	{
		currentAction = currentBox;
		attackDamage = damage;
		DR = NewDR;
		takenAction = actionender;
		clicked = true;
		skillPoints -= SPcost;
		if (skillPoints > 10) skillPoints = 10; //Set the max on skill points at 10

		if (currentAction == "Heavy")
		{
			heavyAttackEffect.play();
		}

		if (currentAction == "Light")
		{
			lightAttackEffect.play();
		}
	}
}

void Player::Update()
{
	//Make sure that health cant overflow
	if (health > 1000)
	{
		health = 1000;
	}
	healthText.setString("Player Health = " + std::to_string((int)health));
	skillText.setString("SP = " + std::to_string((int)skillPoints));

}

std::string Player::getAction()
{
	return currentAction;
}

bool Player::checkAction()
{
	return takenAction;
}


void Player::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
	target.draw(healthText);
	target.draw(skillText);
}

float Player::getDamage()
{
	return attackDamage;
}

void Player::takeDamage(float damage)
{
	health = health - (damage * DR);
}

bool Player::hasClicked()
{
	return clicked;
}

void Player::reset()
{
	clicked = false;
	takenAction = false;
	currentAction = "None";
	DR = 1;
	attackDamage = 0;
}

void Player::Heal(float healValue)
{
	health += healValue;
}

void Player::fullSkills()
{
	skillPoints = 10;
}

float Player::getHealth()
{
	return health;
}

