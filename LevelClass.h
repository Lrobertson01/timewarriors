#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Player.h"
#include "SelectBox.h"
#include "Enemy.h"

class Game;
class LevelClass
{
public:

	LevelClass(Game* newGamePointer);
	void input();
	void update(sf::Time gameTime);
	void drawTo(sf::RenderTarget& Target);
	void spawnEnemy(sf::String enemyName);
private:
	Player playerInstance;
	Game* gamePointer;
	sf::Font newFont; //The font needs to be a readily accessible variable, i found out the hard way if you make it and assign it locally things break upon printing
	std::vector<Enemy> enemyInstances;
	std::vector<SpriteObject> background;
	SpriteObject transition;
	bool isFighting; //bool to check if we're currently within a fight
	bool isLevel; //bool to check if we're inside a level
	int currentFight; //tracks what fight we're in
	int currentLevel; //tracks what level we're in

	//Boxes for your basic meter building attacks
	SelectBox attackBox;
	SelectBox guardBox;
	//Boxes to access your menus
	SelectBox skillBox;
	SelectBox itemBox;
	//Boxes to access your special moves
	SelectBox lightBox;
	SelectBox heavyBox;
	SelectBox counterBox;
	SelectBox backBox;
	//Boxes to access your items
	SelectBox healthBox;
	SelectBox SPBox;
	//Boxes to interact with the menu and continue further into the game
	SelectBox startBox;
	SelectBox continueBox;
	SelectBox quitBox;

	bool turn; // Tracks whether the turn is true (players) or false (AI)
	float mouselag; //Variable to make sure you cant double click an option accidentally
	
	sf::Music ambientMusic;
	sf::Music battleMusic;

	sf::Text loseText;
	sf::Text winText;
	sf::Text gameWon;

	bool gameOver;
};

