#include <SFML/Graphics.hpp>
#include "game.h"

int main()
{
	Game gameInstance;

	gameInstance.runGameLoop();

    return 0;
}