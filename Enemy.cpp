#include "Enemy.h"
#include "AssetManager.h"


Enemy::Enemy(sf::Vector2u screensize, sf::String filename)
	:SpriteObject(AssetManager::RequestTexture("Assets/Enemies/" + filename + ".png"))
	, DR(1)
	, attackDamage(100)
	, superPrep(false)
{
	sprite.setPosition(sf::Vector2f(screensize.x /2 - sprite.getGlobalBounds().width/2, screensize.y/3 - sprite.getGlobalBounds().height/2));

	ifstream textFile;
	string templine;
	textFile.open("Enemies/" + (string)filename + ".txt"); //A text file containing the stats of the enemy in question
	int NumOfLines = 0;

	if (textFile.is_open())
	{
		//To remind myself as much as anyone else. What is happening here is a loop that while the value of getline isnt greater than the amount of lines in text file, it will read each line
		//The value of temp line isnt equal to the line its on, but rather whatever  is present in templine. so if templine is at one, its the value in the first line.
		while (getline(textFile, templine)) //while still reading the file/has not reached the end yet
		{
			if (NumOfLines == 0) 
			{
				enemyHealth = stoi(templine); //the stoi function changes a string to its relative value as an int this is needed as getline will only take a string
				maxHealth = stoi(templine);
			}
			if (NumOfLines == 1)
			{
				normalDamage = stoi(templine);
			}
			if (NumOfLines == 2)
			{
				heavyDamage = stoi(templine);
			}
			if (NumOfLines == 3)
			{
				SuperDamage = stoi(templine);
			}
			if (NumOfLines == 4)
			{
				hintText.setString("Hint: " + templine);
			}
			NumOfLines++; //read the next line
		}

		textFile.close();
	}

	//The reason font is set manually upon each instance of 
	enemyHealthText.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
	enemyHealthText.setString("Enemy Health = 750");
	enemyHealthText.setCharacterSize(32);
	enemyHealthText.setFillColor(sf::Color::White);
	enemyHealthText.setPosition(screensize.x / 3, screensize.y / 10);

	attackText.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
	attackText.setString("");
	attackText.setCharacterSize(64);
	attackText.setFillColor(sf::Color::White);
	attackText.setPosition(screensize.x / 4, screensize.y / 10 - 100);

	damageText.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
	damageText.setString("");
	damageText.setCharacterSize(48);
	damageText.setFillColor(sf::Color::Red);
	damageText.setPosition(sf::Vector2f( sprite.getPosition().x + 250, sprite.getPosition().y));

	hintText.setFont(AssetManager::RequestFont("Assets/Fonts/gameFont.ttf"));
	hintText.setCharacterSize(20);
	hintText.setFillColor(sf::Color::White);
	hintText.setPosition(sf::Vector2f(screensize.x/2 - hintText.getLocalBounds().width/2, screensize.y - 100));

}

void Enemy::Update()
{
	enemyHealthText.setString("Enemy Health = " + std::to_string((int)enemyHealth));
}

void Enemy::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
	target.draw(enemyHealthText);
	target.draw(attackText);
	target.draw(damageText);
	target.draw(hintText);
}

void Enemy::takeDamage(float incomDam)
{
	damageText.setString(std::to_string(int(incomDam * DR)));
	enemyHealth -= (incomDam * DR);
}

float Enemy::Attack()
{
	if (superPrep)
	{
		attackDamage = SuperDamage;
		superPrep = false;
		attackText.setString("SUPER Attack");
	}

	else if (!superPrep) 
	{
		int newRand;
		newRand = rand() % 10 + 1;
		if (newRand <= 6)
		{
			attackDamage = normalDamage;
			attackText.setString("Normal Attack");
		}
		else if (newRand <= 9)
		{
			attackDamage = heavyDamage;
			attackText.setString("Heavy Attack");
		}
		else if (newRand == 10)
		{
			attackDamage = 0;
			superPrep = true;
			attackText.setString("Preparing Super Attack");
		}
	}


	return attackDamage;
}

float Enemy::getHealth()
{
	return enemyHealth;
}

void Enemy::restoreHealth()
{
	enemyHealth = maxHealth;
}
