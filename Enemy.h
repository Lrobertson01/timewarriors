#pragma once
#include "SpriteObject.h"
#include <fstream> //This allows us to open files and read from them, which i will be using to get the enemies statistics
using namespace std;
#include <iostream> 
class Enemy :
    public SpriteObject
{
public:
    Enemy(sf::Vector2u screensize, sf::String filename);
    void Update();
    void DrawTo(sf::RenderTarget& target);
    void takeDamage(float incomDam);
    float Attack();
    float getHealth();
    void restoreHealth();

private:
    float enemyHealth;
    sf::Text enemyHealthText;
    float DR;
    int attackDamage;
    bool superPrep;
    sf::Text attackText;
    sf::Text damageText;
    sf::Text hintText;
    float maxHealth;

    int normalDamage;
    int heavyDamage;
    int SuperDamage;
};

