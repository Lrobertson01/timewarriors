#pragma once
#include "SpriteObject.h"
#include <SFML/Audio.hpp>


class Player :
    public SpriteObject
{
public:
    Player(sf::Vector2u Screensize);

    void Input(sf::FloatRect newBox, sf::String currentBox, float NewDR, float damage, bool actionender, int SPcost);
    void Update();
    std::string getAction();
    bool checkAction();
    void DrawTo(sf::RenderTarget& target);
    float getDamage();
    void takeDamage(float damage);
    bool hasClicked();
    void reset();
    void Heal(float healValue);
    void fullSkills();
    float getHealth();

private:
    std::string currentAction;
    bool takenAction;
    sf::Text healthText;
    float health;
    sf::Font gameFont;
    float DR;
    bool clicked;
    int skillPoints;
    sf::Text skillText;
    float attackDamage;
    sf::SoundBuffer lightSound;
    sf::Sound lightAttackEffect;
    sf::SoundBuffer heavySound;
    sf::Sound heavyAttackEffect;
    sf::SoundBuffer counterSound;
    sf::Sound counterAttackEffect;
};

